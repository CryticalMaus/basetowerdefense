﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;


public class GameMapBehavior : MonoBehaviour {
    public Camera player_camera;
    public Text entity_counter, fps_counter;

    List<GameMap> maps_ = new List<GameMap>();

    int height_ = 32, width_ = 32;

    // Use this for initialization
    void Start () {
        Assert.IsTrue(height_ > 0);
        Assert.IsTrue(width_ > 0);

        Assert.IsNotNull(player_camera);
        Vector3 new_camera_positon = new Vector3(width_ / 2, height_ / 2, -10);
        player_camera.gameObject.transform.position = new_camera_positon;
        player_camera.orthographicSize = 15;
    }
	
	// Update is called once per frame
	void Update () {
        fps_counter.text = "FPS: " + Mathf.FloorToInt(1.0f / Time.deltaTime).ToString();
	}

    public Vector3 GetDirection(Vector3 pos, int path)
    {
        if (path >= maps_.Count)
        {
            return new Vector3();
        }
        return this.maps_[path].GetDirection(pos);
    }
    public void UpdateMapVectors(Vector2Int pos)
    {
        foreach (GameMap path in maps_)
        {
            path.UpdateMapVectors();
        }
    }


    public void Block(Vector2Int pos)
    {
        foreach (GameMap path in maps_)
        {
            path.Block(pos);
        }
        enemy_locations_[pos.y, pos.x].blocked = true;
    }
    public void UnBlock(Vector2Int pos)
    {
        foreach (GameMap path in maps_)
        {
            path.UnBlock(pos);
        }
        enemy_locations_[pos.y, pos.x].blocked = false;
    }



    public struct enemy_location
    {
        public bool blocked;
        public HashSet<Enemy> enemies;
    }
    public enemy_location[,] enemy_locations_;

    public List<enemy_location> GetLocationsInRange(Vector2Int pos, float range)
    {
        List<enemy_location> locations = new List<enemy_location>();

        int flat_range = Mathf.CeilToInt(range);
        float range_squared = Mathf.Pow(flat_range, 2);

        for (int dy = -flat_range; dy <= flat_range; ++dy)
        {
            for (int dx = -flat_range; dx <= flat_range; ++dx)
            {
                int x = pos.x + dx;
                int y = pos.y + dy;
                if (x < 0 || x > width_) continue;
                if (y < 0 || y > height_) continue;

                if (enemy_locations_[y, x].blocked) continue;

                if (Mathf.Pow(dx, 2) + Mathf.Pow(dy, 2) <= range_squared)
                {
                    locations.Add(enemy_locations_[y, x]);
                }
            }
        }

        return locations;
    }

}

