﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using Assets.Enemies;

public class Enemy : MonoBehaviour {
    EnemyBehavior behavior;
    public int Health = 10;
    public static GameMapBehavior map;
    static int EntityCount = 0;
    public int path = 0;

    // Use this for initialization
    protected virtual void Start()
    {
        if (map == null)
        {
            map = GameObject.Find("GameMap").GetComponent<GameMapBehavior>();
        }
        EntityCount += 1;
        map.entity_counter.text = "Entity Count: " + EntityCount.ToString();

        Assert.IsNotNull(map);

        Vector2Int int_pos = new Vector2Int(Mathf.FloorToInt(this.gameObject.transform.position.x), Mathf.FloorToInt(this.gameObject.transform.position.y));
        Enemy.map.enemy_locations_[int_pos.y, int_pos.x].enemies.Add(this);
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (Health <= 0) return;

        float dt = Time.deltaTime;
        Vector3 delta = map.GetDirection(this.gameObject.transform.position, path);
        if (delta.x == 0 && delta.y == 0)
        {
            //this will occur if either the enemy is off the path, or has reached the player base
            Destroy(this.gameObject);
            return;
        }

        Vector2Int before = new Vector2Int(Mathf.FloorToInt(this.gameObject.transform.position.x), Mathf.FloorToInt(this.gameObject.transform.position.y));
        this.gameObject.transform.position += delta * dt * 4;
        Vector2Int after = new Vector2Int(Mathf.FloorToInt(this.gameObject.transform.position.x), Mathf.FloorToInt(this.gameObject.transform.position.y));

        if (before != after)
        {
            Enemy.map.enemy_locations_[before.y, before.x].enemies.Remove(this);
            Enemy.map.enemy_locations_[after.y, after.x].enemies.Add(this);
        }
    }

    protected virtual void OnDestroy()
    {
        --EntityCount;
        map.entity_counter.text = "Entity Count: " + EntityCount.ToString();

        Vector2Int int_pos = new Vector2Int(Mathf.FloorToInt(this.gameObject.transform.position.x), Mathf.FloorToInt(this.gameObject.transform.position.y));
        Enemy.map.enemy_locations_[int_pos.y, int_pos.x].enemies.Remove(this);
    }

    public void DealDamage(int val)
    {
        if (Health <= 0) return;

        Health -= val;
        if (Health <= 0)
        {
            Destroy(this.gameObject);
        }
    }
}
