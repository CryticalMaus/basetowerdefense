﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using Assets.Towers;

public class Tower : MonoBehaviour {
    TowerBehavior behavior;
    public static GameMapBehavior map;
    public float range = 0.1f;
    public float update_delay = 0.2f;
    float update_time_ = 0;
    Vector2Int position_;
    List<GameMapBehavior.enemy_location> target_locations_ = new List<GameMapBehavior.enemy_location>();
    LineRenderer line_render_;

    // Use this for initialization
    void Start()
    {
        if (map == null)
        {
            map = GameObject.Find("GameMap").GetComponent<GameMapBehavior>();
        }
        line_render_ = GetComponent<LineRenderer>();
        Assert.IsNotNull(map);
        Assert.IsNotNull(line_render_);

        position_ = new Vector2Int(Mathf.FloorToInt(this.gameObject.transform.position.x), Mathf.FloorToInt(this.gameObject.transform.position.y));
        UpdateRange(range);
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (Time.fixedTime >= update_time_)
        {
            update_time_ = Time.fixedTime + update_delay;
            Enemy target = GetTarget();
            if (target)
            {
                target.DealDamage(10);
            }
        }
    }

    Enemy GetTarget()
    {
        foreach (GameMapBehavior.enemy_location el in target_locations_)
        {
            foreach (Enemy pt in el.enemies)
            {
                //apply filter... currently none
                return pt;
            }
        }
        return null;
    }

    void UpdateRange(float range)
    {
        this.range = range;
        target_locations_ = map.GetLocationsInRange(position_, range);

        float Theta = 0f;
        const float ThetaScale = 0.01f;
        int radius = Mathf.CeilToInt(range);
        int Size = (int)((1f / ThetaScale) + 1f);
        line_render_.positionCount = Size;
        for (int i = 0; i < Size; i++)
        {
            Theta += (2.0f * Mathf.PI * ThetaScale);
            float x = radius * Mathf.Cos(Theta);
            float y = radius * Mathf.Sin(Theta);
            line_render_.SetPosition(i, new Vector3(x + 0.5f, y + 0.5f, 0));
        }
    }
}
