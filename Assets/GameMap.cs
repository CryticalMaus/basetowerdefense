﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;

class GameMap
{
    struct tile_t
    {
        public bool blocked;
        public Vector2 path;
        public int distance;
    };
    tile_t[,] map_ = null;
    Vector2Int[] source_points_;
    int height_, width_;
    public int height { get { return height_; } }
    public int width { get { return width_; } }

    public GameMap(int height, int width, Vector2Int[] source_points)
    {
        height_ = height;
        width_ = width;
        map_ = new tile_t[height_, width_];
        source_points_ = source_points;

        foreach (Vector2Int e_pos in source_points)
        {
            Assert.IsTrue(e_pos.x < width_);
            Assert.IsTrue(0 <= e_pos.x);
            Assert.IsTrue(e_pos.y < height_);
            Assert.IsTrue(0 <= e_pos.y);
        }

        UpdateMapVectors();
    }

    public void UpdateMapVectors()
    {
        //Debug.DrawLine();
        Assert.IsNotNull(map_);

        bool[,] checklist = new bool[height_, width_];
        Queue<Vector2Int> stack = new Queue<Vector2Int>();
        foreach (Vector2Int vec in source_points_)
        {
            stack.Enqueue(vec);
        }
        while (stack.Count > 0)
        {
            Vector2Int pos = stack.Dequeue();
            int x, y;
            for (int dy = -1; dy <= 1; ++dy)
            {
                y = pos.y + dy;
                if (y < 0 || y >= height_) continue;
                for (int dx = -1; dx <= 1; ++dx)
                {
                    x = pos.x + dx;

                    if (x < 0 || x >= width_) continue;
                    if (dx != 0 && dy != 0) continue;
                    if (checklist[y, x]) continue;
                    checklist[y, x] = true;

                    map_[y, x].path.x = -dx;
                    map_[y, x].path.y = -dy;
                    map_[y, x].distance = map_[pos.y, pos.x].distance + 1;

                    if (map_[y, x].blocked) continue;

                    stack.Enqueue(new Vector2Int(x, y));
                }
            }
        }
    }

    public Vector3 GetDirection(Vector3 pos)
    {
        int y = Mathf.FloorToInt(pos.y);
        int x = Mathf.FloorToInt(pos.x);
        if (x < 0 || x > width_) return new Vector3();
        if (y < 0 || y > height_) return new Vector3();

        return map_[y, x].path;
    }

    public void Block(Vector2Int pos)
    {
        map_[pos.y, pos.x].blocked = true;
    }
    public void UnBlock(Vector2Int pos)
    {
        map_[pos.y, pos.x].blocked = false;
    }
}
